/**
 * Created by reset on 2017/7/14.
 */
let user = require("./User");

let http = require("http");
let url = require("url");
let util = require("util");

let server = http.createServer((req,res)=>{
  res.statusCode = 200;
  res.setHeader("Content-Type","text/html");


  res.end(util.inspect(url.parse(req.url)));

})
server.listen(3000,"127.0.0.1",()=>{
  console.log("服务器已运行");
});
