/**
 * Created by reset on 2017/7/17.
 */
let http = require("http");
let url = require("url");
let util = require("util");
let fs = require("fs");

var server = http.createServer((req,res)=>{


  var pathName = url.parse(req.url).pathname;

  fs.readFile(pathName.substring(1), function (err, data) {
    if(err){
      console.log("err");
    }else{
      res.write(data.toString());
    }
    res.end();
  })

})

server.listen(3001, "127.0.0.1", function () {
  console.log("服务器开启成功~");
})
